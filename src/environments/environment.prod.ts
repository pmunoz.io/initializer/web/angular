export const env = {
  production: true,
  apiUrl: 'http://localhost:3000',
  oauthClient: 'com.example.web',
  oauthSecret: 'WEBSECRET',
  snackBarDuration: 3000,
  snackBarPosition: 'OK',
  snackBarAction: 'top'
};
