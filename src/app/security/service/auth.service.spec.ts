import { HttpClientModule, HttpRequest } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthService } from './auth.service';
import { SIGN_IN_ROUTE_MOCK, USER_MOCK } from '../../user/mock/user.mock';
import { TOKEN_MOCK, HTTP_HEADERS_MOCK, PRINCIPAL_MOCK, RESPONSE_ERROR_LOGIN_MOCK } from '../../../app/security/mock/auth.mock';
import { LOCAL_STORAGE_SESSION } from '../constant/auth.constant';
import { env } from '../../../environments/environment';
import { User } from '../../user/model/user.model';

describe('AuthService', () => {

  let spyRouter: jest.SpyInstance;
  let service: AuthService;
  let backend: HttpTestingController;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [ AuthService ]
    });

    backend = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
    spyRouter = jest.spyOn(router, 'navigate').mockResolvedValue(true);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('service should be created', () => {
    expect(service).toBeTruthy();
    expect(service.URL).toBe(env.apiUrl + '/auth');
  });

  test('should not authenticate with invalid credentials', () => {

    const headers = {
      contentType: '',
      authorization: ''
    };
    let body = {};
    const data = {
      email: USER_MOCK.data.email,
      password: USER_MOCK.data.password
    };

    service.login(data.email, data.password).then( (response) => {
      expect(response).toEqual(false);
    });

    backend.expectOne((req: HttpRequest<any>) => {
      if(req.url === `${service.URL}/login` && req.method === 'POST'){
        body = req.body;
        headers.contentType = req.headers.get('Content-Type');
        headers.authorization = req.headers.get('Authorization');
        return true;
      }
    }, 'POST login to backend')
    .flush(RESPONSE_ERROR_LOGIN_MOCK);

    expect(localStorage.length).toEqual(0);
    expect(body).toMatchObject(data);
    expect(headers).toMatchObject(HTTP_HEADERS_MOCK);
  });

  test('should not get the principal associates to an user', () => {
    localStorage.clear();
    const principal = service.getUser();
    expect(principal).toEqual(null);
  });

  test('should authenticate with a valid form', () => {
    const headers = {
      contentType: '',
      authorization: ''
    };
    let body = {};
    const data = {
      email: USER_MOCK.data.email,
      password: USER_MOCK.data.password
    };

    service.login(data.email, data.password).then( (response) => {
      expect(response).toEqual(true);
    });

    backend.expectOne((req: HttpRequest<any>) => {
      if(req.url === `${service.URL}/login` && req.method === 'POST'){
        body = req.body;
        headers.contentType = req.headers.get('Content-Type');
        headers.authorization = req.headers.get('Authorization');
        return true;
      }
    }, 'POST login to backend')
    .flush(TOKEN_MOCK);

    localStorage.setItem(LOCAL_STORAGE_SESSION, TOKEN_MOCK.accessToken);
    const token = localStorage.getItem(LOCAL_STORAGE_SESSION);

    expect(token).toEqual(TOKEN_MOCK.accessToken);
    expect(body).toMatchObject(data);
    expect(headers).toMatchObject(HTTP_HEADERS_MOCK);
  });

  test('should register a user', () => {
    let user: {};
    const rUser: User = {
      name: USER_MOCK.data.name,
      password: USER_MOCK.data.password,
      email: USER_MOCK.data.email
    }

    service.registerUser(rUser).subscribe(u => user = u);
    backend.expectOne((req: HttpRequest<any>) => {
      return req.url === `${service.URL}/register`
        && req.method === 'POST'
    }, 'POST user to backend')
    .flush(USER_MOCK);

    expect(user).toBe(USER_MOCK);
  });

  test('should ask if the user is authenticated and the result is true', async () => {
    localStorage.setItem(LOCAL_STORAGE_SESSION, TOKEN_MOCK.accessToken);
    const authenticated = service.isAuthenticated();
    expect(authenticated).toBeTruthy();
  });

  test('should get the principal associates to an user', () => {
    localStorage.setItem(LOCAL_STORAGE_SESSION, TOKEN_MOCK.accessToken);
    const user = service.getUser();
    expect(user.principal).toEqual(PRINCIPAL_MOCK);
  });

  test('should ask if the user is authenticated and the result is false', async () => {
    localStorage.clear();
    const response = service.isAuthenticated();
    expect(response).toBeNull();
  });

  test('should expirate the token', () => {
    localStorage.setItem(LOCAL_STORAGE_SESSION, TOKEN_MOCK.accessToken);
    jest.useFakeTimers();

    service.setTokenExpiration(1);

    jest.advanceTimersByTime(1000);
    expect(localStorage.length).toEqual(0);
    expect(spyRouter).toHaveBeenCalled();
  });

  test('should log out', () => {
    localStorage.setItem(LOCAL_STORAGE_SESSION, TOKEN_MOCK.accessToken);
    service.logout();
    expect(spyRouter).toHaveBeenCalledWith([SIGN_IN_ROUTE_MOCK]);
    expect(localStorage.length).toBe(0);
  });
});
