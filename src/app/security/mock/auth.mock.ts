import { of } from 'rxjs';
import { env } from '../../../environments/environment';
import { USER_MOCK, RESPONSE_ERROR } from '../../user/mock/user.mock';
export const AUTH_ERROR_MESSAGE_MOCK = 'invalid credentials'
export const LOGIN_FORM_MOCK = {
  email: 'name1@hotmaail.com',
  password: 'pass1'
};

export const LOGIN_FORM_INVALID_MOCK = {
    email: 'name1hotmail.com',
    password: 'pass1'
};

export const FORM_EMAIL_INVALID_MOCK = {
  name: 'name1',
  email: 'name1hotmail.com',
  password: 'pass1234567'
};

export const FORM_PASSWORD_INVALID_MOCK = {
  name: 'name1',
  email: 'name1@hotmail.com',
  password: 'pass1'
};

export const TOKEN_MOCK = {
  accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmluY2lwYWwiOnsiZW1haWwiOiJleGFtcGxlQGV4YW1wbGUuY29tIiwicm9sZSI6ImJhc2ljVXNlciIsImlkIjoiZXhhbXBsZUlkIn0sImlhdCI6MTU4NDY3ODEzNiwiZXhwIjoxNTg0Njc4NDM2fQ.VU8a7slrV5h0YUtnSwcEjb0o9TbTnnRTqn6AqnxvyiQ'
};

export const LOCAL_STORAGE_TOKEN_MOCK = {
  key: 'accessToken',
  value: 'valueTokenLocalStorage'
};

export const LOCAL_STORAGE_USER_MOCK = {
  key: 'user',
  value: 'valueUserLocalSotrage'
};

export const PRINCIPAL_MOCK = {
  email: 'example@example.com',
  role: 'basicUser',
  id: 'exampleId'
}

export const HTTP_HEADERS_MOCK = {
  contentType: 'application/json',
  authorization: 'Basic ' + btoa(env.oauthClient + ':' + env.oauthSecret)
};

export const RESPONSE_ERROR_LOGIN_MOCK ={
  statusCode: 401,
  error: 'ERROR',
  message: '',
  data: null
};

export class MockService {
  static registerUser = jest.fn().mockReturnValue(of(USER_MOCK));
  static login = jest.fn().mockReturnValue(of(true));
}

export class MockServiceFail {
  static registerUser = jest.fn().mockReturnValue(of(RESPONSE_ERROR));
  static login = jest.fn().mockReturnValue(false);
};