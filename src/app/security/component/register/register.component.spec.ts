import { ReactiveFormsModule } from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import {
  CREATE_USER_MOCK,
  LIST_USERS_ROUTE_MOCK
} from '../../../user/mock/user.mock';
import { FORM_EMAIL_INVALID_MOCK, FORM_PASSWORD_INVALID_MOCK } from '../../mock/auth.mock';

import { RegisterComponent } from './register.component';
import { MockService, MockServiceFail } from '../../mock/auth.mock';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AuthService } from '../../service/auth.service';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let spyRouter: jest.SpyInstance;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports:[
        BrowserAnimationsModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: AuthService, useValue: MockService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(inject([Router], async (_router: Router) => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.inject(AuthService);
    spyRouter = jest.spyOn(_router, 'navigate').mockResolvedValue(true);
  }));

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('component should be created ', () => {
    expect(component).toBeTruthy();
  });

  test('should not create and have invalid form', () => {
    expect(component.registerForm).toBeTruthy();
    expect(component.registerForm.valid).toBeFalsy();
  });

  test('should validate form email', () => {
    component.registerForm.patchValue(FORM_EMAIL_INVALID_MOCK);
    expect(component.registerForm.valid).toBeFalsy();
  });

  test('should validate form password', () => {
    component.registerForm.patchValue(FORM_PASSWORD_INVALID_MOCK);
    expect(component.registerForm.valid).toBeFalsy();
  });

  test('should register an user if the form is valid', async () => {
    component.registerForm.patchValue(CREATE_USER_MOCK);

    await component.saveUser();
    expect(authService.registerUser).toHaveBeenCalled();
    expect(spyRouter).toHaveBeenCalledWith([LIST_USERS_ROUTE_MOCK]);
    expect(authService.registerUser).toHaveBeenCalledWith(CREATE_USER_MOCK);
  });

});

describe('RegisterComponentFail', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let spyRouter: jest.SpyInstance;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports:[
        BrowserAnimationsModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: AuthService, useValue: MockServiceFail }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(inject([Router], async (_router: Router) => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.inject(AuthService);
    spyRouter = jest.spyOn(_router, 'navigate').mockResolvedValue(true);
  }));

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should not save an user if the form is valid', async () => {
    component.registerForm.patchValue(CREATE_USER_MOCK);

    await component.saveUser();
    expect(spyRouter).not.toHaveBeenCalled();
    expect(authService.registerUser).toHaveBeenCalledTimes(1);
    expect(authService.registerUser).toHaveBeenCalledWith(CREATE_USER_MOCK);
  });

  test('should not register an user if the form is invalid', async () => {
    CREATE_USER_MOCK.name = null;
    component.registerForm.patchValue(CREATE_USER_MOCK);

    await component.saveUser();

    expect(spyRouter).not.toHaveBeenCalled();
    expect(component.registerForm.valid).toBeFalsy();
    expect(authService.registerUser).not.toHaveBeenCalled();
  });

});