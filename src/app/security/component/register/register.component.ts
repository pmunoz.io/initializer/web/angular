import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

import { AUTH_FORM, REGISTER_USER_MESSAGES } from '../../constant/auth.constant'
import { FORM, FORM_ERROR_MESSAGES } from '../../../../shared/constant/form.constant';
import { AuthService } from '../../service/auth.service';
import { env } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  EMAIL = AUTH_FORM.REGISTER.LABEL.EMAIL;
  NAME = AUTH_FORM.REGISTER.LABEL.NAME;
  PASSWORD = AUTH_FORM.REGISTER.LABEL.PASSWORD;
  REGISTER_BTN = AUTH_FORM.REGISTER.BUTTON.REGISTER;
  TITLE = AUTH_FORM.REGISTER.LABEL.TITLE;
  CANCEL_BTN = FORM.GENERAL_BUTTON.CANCEL;

  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
    private router: Router,
    private authService: AuthService,
  ) { };

  ngOnInit() {
    this.buildFormRegister();
  };

  buildFormRegister() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  };

  async saveUser() {
    if (this.registerForm.valid) {
      const createUser = this.registerForm.value;
      const resp = await this.authService.registerUser(createUser).toPromise();
      if (resp.data) {
        const { email, password } = createUser;
        const response = await this.authService.login(email, password);
        if (response) {
          this.router.navigate(['/home/users/list-users']);
        } else {
          this.matSnackBar.open(REGISTER_USER_MESSAGES.ERROR, env.snackBarAction,
            { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
        }
      } else {
        this.matSnackBar.open(REGISTER_USER_MESSAGES.ERROR, env.snackBarAction,
          { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
      }
    } else {
      this.matSnackBar.open(FORM_ERROR_MESSAGES.FORM_ERRORS, env.snackBarAction,
        { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
    }
  };
}