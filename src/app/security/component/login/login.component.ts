import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

import { AuthService } from '../../service/auth.service';
import { AUTH_MESSAGES, AUTH_FORM } from '../../constant/auth.constant';
import { env } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  EMAIL = AUTH_FORM.LOGIN.LABEL.EMAIL;
  LOGIN_BTN = AUTH_FORM.LOGIN.BUTTON.LOGIN;
  PASSWORD = AUTH_FORM.LOGIN.LABEL.PASSWORD;
  REGISTER_BTN = AUTH_FORM.LOGIN.BUTTON.REGISTER;
  TITLE = AUTH_FORM.LOGIN.LABEL.TITLE;

  loginForm: FormGroup;
  response: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar
  ) { };

   ngOnInit() {
    this.buildLoginForm();
  };

  buildLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  };

  async login() {
    if(this.loginForm.valid){
      const { email, password } = this.loginForm.value;
      this.response = await this.authService.login(email, password);
      if (this.response) {
        this.router.navigate(['/home']);
      }else{
        this.matSnackBar.open(
          AUTH_MESSAGES.INVALID_AUTHENTICATION, env.snackBarAction,
          { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
      }
    }else{
      this.matSnackBar.open(
        AUTH_MESSAGES.EMPTY_FORM, env.snackBarAction,
        { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
    }
  };
}
