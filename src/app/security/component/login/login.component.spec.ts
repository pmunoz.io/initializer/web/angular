import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { Router } from '@angular/router';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { LoginComponent } from './login.component';
import { AuthService } from '../../service/auth.service';
import { LOGIN_FORM_MOCK, LOGIN_FORM_INVALID_MOCK, MockService, MockServiceFail } from '../../mock/auth.mock';
import { LIST_USERS_ROUTE_MOCK } from '../../../user/mock/user.mock';

describe('LoginComponent', () => {

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;
  let spyRouter: jest.SpyInstance;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        BrowserAnimationsModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: AuthService, useValue: MockService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(inject([Router], async (_router: Router) => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.inject(AuthService);
    spyRouter = jest.spyOn(_router, 'navigate').mockResolvedValue(true);
  }));

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should not authenticate and have invalid form', () => {
    expect(component).toBeTruthy();
    expect(component.loginForm).toBeTruthy();
    expect(component.loginForm.valid).toBeFalsy();
  });

  test('should validate form fields', () => {
    component.loginForm.patchValue(LOGIN_FORM_MOCK);
    expect(component.loginForm.valid).toBeTruthy();
  });

  test('should validate form email', () => {
    component.loginForm.patchValue(LOGIN_FORM_INVALID_MOCK);
    expect(component.loginForm.valid).toBeFalsy();
  });

  test('should perform login', async () => {
    component.loginForm.patchValue(LOGIN_FORM_MOCK);
    await component.login();
    expect(authService.login).toHaveBeenCalled();
    expect(spyRouter).toHaveBeenCalledWith([LIST_USERS_ROUTE_MOCK]);

  });

  test('should not perform login', async () => {
    component.loginForm.patchValue(LOGIN_FORM_MOCK);
    const spyService = jest.spyOn(authService, 'login').mockResolvedValue(false);

    await component.login();

    expect(spyService).toHaveBeenCalled();
    expect(spyRouter).not.toHaveBeenCalledWith([LIST_USERS_ROUTE_MOCK]);

  });

  test('should not get a login if fields are empty', async () => {
    await component.login();

    expect(spyRouter).not.toHaveBeenCalled();
    expect(component.loginForm.valid).toBeFalsy();
  });

});

describe('LoginComponentFail', () => {

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;
  let spyRouter: jest.SpyInstance;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        BrowserAnimationsModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: AuthService, useValue: MockServiceFail }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(inject([Router], async (_router: Router) => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.inject(AuthService);
    spyRouter = jest.spyOn(_router, 'navigate').mockResolvedValue(true);
  }));

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should not perform login', async () => {
    component.loginForm.patchValue(LOGIN_FORM_MOCK);
    await component.login();
    expect(authService.login).toHaveBeenCalled();
    expect(spyRouter).not.toHaveBeenCalledWith([LIST_USERS_ROUTE_MOCK]);
  });

  test('should not get a login if fields are empty', async () => {
    await component.login();
    expect(spyRouter).not.toHaveBeenCalled();
    expect(component.loginForm.valid).toBeFalsy();
  });

});