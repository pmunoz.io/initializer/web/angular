import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';

import { AuthService } from './../service/auth.service';
import { Observable } from 'rxjs';

type CanActivateReturnType = boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>;
@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate{

  constructor( private authService: AuthService ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): CanActivateReturnType{
    if(this.authService.isAuthenticated()){
      return true;
    }else{
      this.authService.logout();
      return false;
    }
  }
}