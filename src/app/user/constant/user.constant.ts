export const USER_FORM = {
  UPDATE_USER: {
    LABEL: {
      EMAIL: 'Email *',
      NAME: 'Full name *',
      PASSWORD: 'Password *',
      TITLE: 'Update user'
    }
  },
  LIST_USERS: {
    LABEL: {
      EMAIL: 'Email',
      NAME: 'Name',
      TITLE: 'User List'
    }
  },
}

export const UPDATE_USER_MESSAGES = {
    SUCCESS: 'User has been successfully updated',
    ERROR: 'An error has occurred while the user was being updated.',
}
