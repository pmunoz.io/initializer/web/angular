import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';

import { ListUsersComponent } from './list-users.component';
import { UserService } from '../../service/user.service';
import { USER_LSIT_MOCK, USER_ID } from '../../mock/user.mock';
import { MockService } from '../../mock/user.mock';

describe('ListUsersComponent', () => {

  let component: ListUsersComponent;
  let fixture: ComponentFixture<ListUsersComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListUsersComponent],
      imports: [
        MatTableModule,
        RouterModule,
        HttpClientModule
      ],
      providers: [
        { provide: UserService, useValue: MockService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userService = TestBed.inject(UserService);
  });

  test('component should be created', () => {
    expect(component).toBeTruthy();
  });

  test('should change the state of an user', async () =>{
    await component.deleteUser(USER_ID);
    expect(userService.deleteUser).toHaveBeenCalled();
    expect(userService.deleteUser).toHaveBeenCalledWith(USER_ID);
  });

  test('should return all users', async () =>{
    await component.getUsers();
    expect(userService.getUsers).toHaveBeenCalled();
    expect(component.users).toEqual(USER_LSIT_MOCK.data);
  });


});
