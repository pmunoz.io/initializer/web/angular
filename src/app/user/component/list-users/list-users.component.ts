import { Component, OnInit } from '@angular/core';

import { User } from '../../model/user.model';
import { UserService } from '../../service/user.service';
import { FORM } from '../../../../shared/constant/form.constant';
import { USER_FORM } from '../../constant/user.constant';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  DELETE_BTN = FORM.GENERAL_BUTTON.DELETE;
  UPDATE_BTN = FORM.GENERAL_BUTTON.UPDATE;
  EMAIL = USER_FORM.LIST_USERS.LABEL.EMAIL;
  NAME = USER_FORM.LIST_USERS.LABEL.NAME;
  TITLE = USER_FORM.LIST_USERS.LABEL.TITLE;

  users: User[] = [];
  displayedColumns: string[] = ['name', 'email', 'button'];

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  async getUsers() {
    this.users = (await this.userService.getUsers().toPromise()).data;
  }

  async deleteUser(id: string) {
    await this.userService.deleteUser(id).toPromise();
    this.getUsers();
  }
}
