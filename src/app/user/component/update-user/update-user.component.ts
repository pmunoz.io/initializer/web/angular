import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

import { USER_FORM, UPDATE_USER_MESSAGES } from '../../constant/user.constant';
import { FORM, FORM_ERROR_MESSAGES } from '../../../../shared/constant/form.constant';
import { User } from '../../model/user.model';
import { UserService } from '../../service/user.service';
import { env } from 'src/environments/environment';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

  EMAIL = USER_FORM.UPDATE_USER.LABEL.EMAIL;
  NAME = USER_FORM.UPDATE_USER.LABEL.NAME;
  PASSWORD = USER_FORM.UPDATE_USER.LABEL.PASSWORD;
  SAVE_BTN = FORM.GENERAL_BUTTON.SAVE;
  TITLE = USER_FORM.UPDATE_USER.LABEL.TITLE;

  updateUserForm: FormGroup;
  id: string;
  user: User;

  constructor(
    private userService: UserService,
    private router: Router,
    private matSnackBar: MatSnackBar,
    private activateRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.id = this.activateRoute.snapshot.paramMap.get('id');
    this.buildFormUpdateUser();
    this.getUser();
  }

  buildFormUpdateUser() {
    this.updateUserForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  };

  async getUser() {
    this.user = (await this.userService.getUser(this.id).toPromise()).data;
  };

  async updateUser() {
    if (this.updateUserForm.valid) {
      const userToUpdate = this.updateUserForm.value;
      const userUpdated = await this.userService.updateUser(this.id, userToUpdate).toPromise();

      if (userUpdated.data) {
        this.router.navigate(['/home/users/list-users'])
        this.matSnackBar.open(UPDATE_USER_MESSAGES.SUCCESS, env.snackBarAction,
          { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
      } else {
        this.matSnackBar.open(UPDATE_USER_MESSAGES.SUCCESS, env.snackBarAction,
          { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
      }
    } else {
      this.matSnackBar.open(FORM_ERROR_MESSAGES.FORM_ERRORS, env.snackBarAction,
        { duration: env.snackBarDuration, verticalPosition: env.snackBarPosition as MatSnackBarVerticalPosition });
    }
  };
}
