import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing';

import { MatSnackBarModule } from '@angular/material/snack-bar';

import { UpdateUserComponent } from './update-user.component';
import { UserService } from '../../service/user.service';
import { Router } from '@angular/router';
import { USER_MOCK, CREATE_USER_MOCK, LIST_USERS_ROUTE_MOCK, MockService, MockServiceFail } from '../../mock/user.mock';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FORM_EMAIL_INVALID_MOCK, FORM_PASSWORD_INVALID_MOCK } from '../../../security/mock/auth.mock';

describe('UpdateUserComponent', () => {
  let component: UpdateUserComponent;
  let fixture: ComponentFixture<UpdateUserComponent>;
  let userService: UserService;
  let spyRouter: jest.SpyInstance;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateUserComponent],
      imports: [
        BrowserAnimationsModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: UserService, useValue: MockService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(inject([ Router ], ( _router: Router ) => {
    fixture = TestBed.createComponent(UpdateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userService = TestBed.inject(UserService);
    spyRouter = jest.spyOn(_router, 'navigate').mockResolvedValue(true);
  }));

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('component should be created ', async () => {
    expect(component).toBeTruthy();
  });

  test('should find an user', async () => {
    await component.getUser();
    expect(userService.getUser).toHaveBeenCalled();
    expect(component.user).toEqual(USER_MOCK.data);
  });

  test('should not update and have invalid form', () => {
    expect(component).toBeTruthy();
    expect(component.updateUserForm).toBeTruthy();
    expect(component.updateUserForm.valid).toBeFalsy();
  });

  test('should validate form email', () => {
    component.updateUserForm.patchValue(FORM_EMAIL_INVALID_MOCK);
    expect(component.updateUserForm.valid).toBeFalsy();
  });

  test('should validate form password', () => {
    component.updateUserForm.patchValue(FORM_PASSWORD_INVALID_MOCK);
    expect(component.updateUserForm.valid).toBeFalsy();
  });

  test('should update an user if the form is valid', async () => {
    CREATE_USER_MOCK.name = 'name1'

    component.updateUserForm.patchValue(CREATE_USER_MOCK);

    component.id = CREATE_USER_MOCK._id;
    component.user = CREATE_USER_MOCK;

    await component.updateUser();

    expect(spyRouter).toHaveBeenCalledWith([LIST_USERS_ROUTE_MOCK]);
    expect(userService.getUser).toHaveBeenCalled();
    expect(userService.updateUser).toHaveBeenCalled();
    expect(userService.updateUser).toHaveBeenCalledWith(CREATE_USER_MOCK._id, CREATE_USER_MOCK);
  });

});


describe('UpdateUserComponent', () => {
  let component: UpdateUserComponent;
  let fixture: ComponentFixture<UpdateUserComponent>;
  let userService: UserService;
  let spyRouter: jest.SpyInstance;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateUserComponent],
      imports: [
        BrowserAnimationsModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [{
        provide: UserService,
        useValue: MockServiceFail
      }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(inject([ Router ], ( _router: Router ) => {
    fixture = TestBed.createComponent(UpdateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userService = TestBed.inject(UserService);
    spyRouter = jest.spyOn(_router, 'navigate').mockResolvedValue(true);
  }));

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should not update an user', async () => {
    CREATE_USER_MOCK.name = null;
    component.updateUserForm.patchValue(CREATE_USER_MOCK);

    await component.updateUser();

    expect(userService.getUser).toHaveBeenCalled();
    expect(spyRouter).not.toHaveBeenCalled();
    expect(component.updateUserForm.valid).toBeFalsy();
  });

  test('should not update an user is the form is valid', async () => {
    CREATE_USER_MOCK.name = 'name1'

    component.updateUserForm.patchValue(CREATE_USER_MOCK);

    component.id = CREATE_USER_MOCK._id;
    component.user = CREATE_USER_MOCK;

    await component.updateUser();

    expect(userService.updateUser).toHaveBeenCalled();
    expect(userService.getUser).toHaveBeenCalled();
    expect(spyRouter).not.toHaveBeenCalled();
    expect(component.updateUserForm.valid).toBeTruthy();
  });
});