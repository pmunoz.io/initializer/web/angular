import { of } from 'rxjs';
import { ResponseObject } from '../../../shared/model/response-object.model';
import { User } from '../model/user.model';

export const USER_MOCK: ResponseObject<User> = {
  statusCode: 200,
  error: '',
  message: '',
  data: {
    _id: '5e6158e0ea61dc53cf1e62f3',
    name: 'Mock User',
    password: '12345678',
    email: 'mock@mail.com',
    active: true
  }
};

export const USER_LSIT_MOCK: ResponseObject<User[]> = {
  statusCode: 200,
  error: '',
  message: 'Success',
  data: [{
    _id: '5e6158e0ea61dc53cf1e62f3',
    name: 'Mock User A',
    password: '12345678',
    email: 'mockA@mail.com',
    active: true
  },
  {
    _id: '5e6158e0eaf1e62f361dc53c',
    name: 'Mock User B',
    password: '90123456',
    email: 'mockB@mail.com',
    active: true
  },
  {
    _id: 'e0ea61dc53cf1e62f35e6158',
    name: 'Mock User C',
    password: '87654321',
    email: 'mockC@mail.com',
    active: true
  }]
};

export const DELETE_USER_MOCK: ResponseObject<User> = {
  statusCode: 200,
  error: '',
  message: '',
  data: {
    _id: '5e6158e0ea61dc53cf1e62f3',
    name: 'Mock User',
    password: '12345678',
    email: 'mock@mail.com',
    active: false
  }
};

export const USER_AUTH_MOCK = {
  SUCCESS: {
    email: 'example@example.com',
    password: 'passexample'
  }
};

export const CREATE_USER_MOCK: User = {
  name: 'name1',
  password: '12345678',
  email: 'mock@mail.com'
};

export const CREATE_USER_MOCK_PLAIN: User = {
  name: 'example',
  email: 'example@example.com',
  password: 'passexample'
};

export const RESPONSE_ERROR: ResponseObject<User> = {
  statusCode: 400,
  error: 'ERROR',
  message: '',
  data: null
};

export const SIGN_IN_ROUTE_MOCK = '/sign-in';
export const LIST_USERS_ROUTE_MOCK = '/home/users/list-users';
export const USER_ID = '5e6158e0ea61dc53cf1e62f3';

export class MockService {
  static getUsers = jest.fn().mockReturnValue(of(USER_LSIT_MOCK));
  static getUser = jest.fn().mockReturnValue(of(USER_MOCK));
  static updateUser = jest.fn().mockReturnValue(of(USER_MOCK));
  static deleteUser = jest.fn().mockReturnValue(of(DELETE_USER_MOCK));
}

export class MockServiceFail {
  static getUsers = jest.fn().mockReturnValue(of(RESPONSE_ERROR));
  static getUser = jest.fn().mockReturnValue(of(RESPONSE_ERROR));
  static updateUser = jest.fn().mockReturnValue(of(RESPONSE_ERROR));
  static deleteUser = jest.fn().mockReturnValue(of(RESPONSE_ERROR));
};
