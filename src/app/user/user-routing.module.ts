import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateUserComponent } from './component/update-user/update-user.component';
import { ListUsersComponent } from './component/list-users/list-users.component';

const routes: Routes = [
  { path: '', redirectTo: 'list-users', pathMatch: 'full' },
  { path: 'edit/:id', component: UpdateUserComponent },
  { path: 'list-users', component: ListUsersComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }