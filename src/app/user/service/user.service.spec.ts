import { HttpClientModule, HttpRequest } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { UserService } from './user.service';
import { USER_LSIT_MOCK, USER_MOCK } from '../mock/user.mock';
import { User } from '../model/user.model';
import { env } from '../../../environments/environment';


describe('UserService', () => {
  let backend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        UserService
      ]
    });

    backend = TestBed.inject(HttpTestingController);
  });

  test('service should be created', () => {
    const service: UserService = TestBed.inject(UserService);

    expect(service).toBeTruthy();
    expect(service.USER_URL).toBe(env.apiUrl + '/user');
  });

  test('should call the GET api and return all users', () => {
    let users: {};
    const service: UserService = TestBed.inject(UserService);

    service.getUsers().subscribe(u => users = u);
    backend.expectOne((req: HttpRequest<any>) => {
      return req.url === `${service.USER_URL}`
        && req.method === 'GET'
    }, 'GET all users from backend')
    .flush(USER_LSIT_MOCK);

    expect(users).toBe(USER_LSIT_MOCK);
  });

  test('should call the GET api and return user by id', () => {
    let user: {};
    const userId = USER_MOCK.data._id;
    const service: UserService = TestBed.inject(UserService);

    service.getUser(userId).subscribe(u => user = u);
    backend.expectOne((req: HttpRequest<any>) => {
      return req.url === `${service.USER_URL}/${userId}`
        && req.method === 'GET'
    }, 'GET users by id from backend')
    .flush(USER_MOCK);

    expect(user).toBe(USER_MOCK);
  });

  test('should call the PUT api by id and return updated user', () => {
    let user: {};
    const userId = USER_MOCK.data._id;
    const rUser: User = {
      name: USER_MOCK.data.name,
      password: USER_MOCK.data.password,
      email: USER_MOCK.data.email
    }
    const service: UserService = TestBed.inject(UserService);

    service.updateUser(userId, rUser).subscribe(u => user = u);
    backend.expectOne((req: HttpRequest<any>) => {
      return req.url === `${service.USER_URL}/${userId}`
        && req.method === 'PUT'
    }, 'UPDATE user from backend')
    .flush(USER_MOCK);

    expect(user).toBe(USER_MOCK);
  });

  test('should call the DELETE api and return registered user', () => {
    let user: {};
    const userId = USER_MOCK.data._id;
    const service: UserService = TestBed.inject(UserService);

    service.deleteUser(userId).subscribe(u => user = u);
    backend.expectOne((req: HttpRequest<any>) => {
      return req.url === `${service.USER_URL}/${userId}`
        && req.method === 'DELETE'
    }, 'DELETE user to backend')
    .flush(USER_MOCK);

    expect(user).toBe(USER_MOCK);
  });
});
