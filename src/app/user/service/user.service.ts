import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { ResponseObject } from 'src/shared/model/response-object.model';
import { User } from '../model/user.model';
import { env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  USER_URL = env.apiUrl + '/user';

  constructor(private http: HttpClient) { }

  getUsers(): Observable<ResponseObject<User[]>> {
    return this.http.get<ResponseObject<User[]>>(`${this.USER_URL}`);
  }

  getUser(id: string): Observable<ResponseObject<User>> {
    return this.http.get<ResponseObject<User>>(`${this.USER_URL}/${id}`);
  }

  updateUser(id: string, user: User): Observable<ResponseObject<User>> {
    return this.http.put<ResponseObject<User>>(`${this.USER_URL}/${id}`, user);
  }

  deleteUser(id: string): Observable<ResponseObject<User>> {
    return this.http.delete<ResponseObject<User>>(`${this.USER_URL}/${id}`);
  }

}
