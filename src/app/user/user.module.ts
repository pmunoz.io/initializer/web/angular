import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';

import { ListUsersComponent } from './component/list-users/list-users.component';
import { UpdateUserComponent } from './component/update-user/update-user.component';
import { UserService } from './service/user.service';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  declarations: [
    ListUsersComponent,
    UpdateUserComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatTableModule,
    ReactiveFormsModule,
    UserRoutingModule
  ],
  providers: [UserService],
})

export class UserModule { }