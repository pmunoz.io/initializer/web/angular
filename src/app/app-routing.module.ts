import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './security/component/login/login.component';
import { RegisterComponent } from './security/component/register/register.component';
import { AnonGuard } from './security/guard/anon.guard';

const routes: Routes = [
  { path: '', redirectTo: 'sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: LoginComponent, canActivate: [AnonGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AnonGuard] },
  { path: '**', redirectTo: 'sign-in' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
