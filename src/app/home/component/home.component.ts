import { Component, OnInit, NgZone } from '@angular/core';

import { HOME_FORM } from '../constant/home.constant';
import { AuthService } from '../../security/service/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  LIST_USERS_BTN = HOME_FORM.HOME.BUTTON.LIST_USER;
  MENU = HOME_FORM.HOME.LABEL.MENU;
  TITLE = HOME_FORM.HOME.LABEL.TITLE;
  LOGOUT = HOME_FORM.HOME.LABEL.LOGOUT;

  events: string[] = [];
  opened: boolean;

  constructor(private authService: AuthService, private ngZone:NgZone) { }

  ngOnInit(): void {
  }

  logout(){
    this.ngZone.run(()=>this.authService.logout());
  }

}
