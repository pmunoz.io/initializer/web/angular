import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { LIST_USER, MENU, TITLE, WORK_QUEUE } from '../mock/home.mock';
import { LOCAL_STORAGE_SESSION } from '../../security/constant/auth.constant';
import { TOKEN_MOCK } from '../../security/mock/auth.mock';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatDividerModule,
        MatListModule,
        RouterModule,
        RouterTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should have the same values', () => {
    expect(component.LIST_USERS_BTN).toBe(LIST_USER);
    expect(component.MENU).toBe(MENU);
    expect(component.TITLE).toBe(TITLE);
  });

  test('should logout', () => {
    localStorage.setItem(LOCAL_STORAGE_SESSION, TOKEN_MOCK.accessToken);
    component.logout();
    expect(localStorage.length).toBe(0);
  });
});
